export interface HttpInterface {
    body:any;
    products:any;
    banner_images:any;
    home_reviews:any;
    homesettings:any;
    categories:categoriesDetails;
    section1_images:any;
    press_image_url:string;
    recycle_image_url:string;
    user_details:userDetails;
    status_code:string;
    msg:any;
    allAddress:any;
    default_add_details:defaultAddressDetails;
    total_products:any;
    price_filters:priceFilters;
    category_filters:categoryFilters;
    minprice:any;
    maxprice:any;
    product_details:productDetails;
    product_images:productImages;
    product_technical_features:productTechnicalFeatures;
    product_features:productFeatures;
    breadcrum_links:breadcrumLinks;
    key_features:keyFeatures;
    subcategory:subcategoryDetails;
    forgot_details:ForgotDetails;
    product_reviews:productReviewDetails;
}
interface productReviewDetails{
    rating_wise_percent:ratingPercentage;
    overAll:string;
    created_by:Number;
    created_date:String;
    email_id:String;
    modify_by:Number;
    modify_date:String;
    name:string;
    product_id:Number;
    rating:Number;
    review_content:string;
    review_title:string;
    reviews_id:Number;
    status:string;
    submission_date:string;
    user_id:Number;
    total_user:Number;
}
interface ratingPercentage{
    1:any;
    2:any;
    3:any;
    4:any;
    5:any;
}
interface ForgotDetails{
    forgot_password_id:number;
    user_id:Number;
    forgot_key:string;
    created_on:Date;

}
interface breadcrumLinks{
    product_id:any;
    category_name:string;
    category_link:string;
    subcategory_name:string;
    subcategory_link:string;

}
interface keyFeatures{
    product_short_feature_id:any;
    short_features_title:string;
}
interface productFeatures{
    features_description:string;
    features_title:string;
    features_images:featuresImages;
}
interface featuresImages{
    features_images:string;
    features_images_path:string;
    product_features_id:string;
    product_features_image_id:any;
}
interface categoryFilters{
    category_filter_id:number;
    category_filter_name:string;
    category_id:number;
}
interface priceFilters{
    min:string;
    max:string;
}
interface categoriesDetails{
    category_id: string;
    category_name: string;
    link: string;
    subcategories:subcategoryDetails;
}

interface subcategoryDetails{
    subCategoryLink:string;
    category_link:string;
    subcategory_link:string;
    subcategory_id:string;
    subcategory_name:string;
    subcategory_image:string;
    subcategory_image_path:string;
}

interface productDetails{
    product_id:string;
    product_model:string;
    product_sku:string;
    product_name:string;
    product_code:string;
    product_warranty:string;
    product_weight:string;
    short_description:string;
    product_description_val:string;
}

interface productImages{
    imagename:string;
    product_big_image_url:string;
    product_thumbnail_image:string;
    product_thumbnail_image_url:string;
}

interface productTechnicalFeatures{
    attribute_group_id:number;
    attribute_group_name:string;
    technical_attributes:technicalAttributes;
}

interface technicalAttributes{
    attribute_master_id:number;
    attribute_name:string;
    attribute_value:string;
}

interface userDetails{
    user_id:number;
    first_name:string;
    last_name:string;
    full_name:string;
    email_id:string;
    gender:string;
    phone_no:string;
    address:string;
    date_of_birth:string;
    default_address:string;
}

interface defaultAddressDetails{
    address_id:string;
    address_title:string;
    user_id:string;
    name_add:string;
    email_id:string;
    street_add:string;
    street_add2:string;
    landmark_add:string;
    city_add:string;
    state_add:string;
    country_add:string;
    pincode_add:string;
    phone_add:string;
    shipping_address_title:string;
    shipping_name_add:string;
    shipping_email_id:string;
    shipping_street_add:string;
    shipping_street_add2:string;
    shipping_landmark_add:string;
    shipping_city_add:string;
    shipping_state_add:string;
    shipping_country_add:string;
    shipping_pincode_add:string;
    shipping_phone_add:string;
    default_add:string;
    same_address:string;
    status:string;
}