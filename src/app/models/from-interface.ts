export interface FormInterface {
    accountInformationForm:accoutnInformation;
    loginForm:loginForm;
    registerForm:registerForm;
    contact_details:accoutnInformation;
    address_details:addressDetails;
    filterForm:filterForms;
}

export interface filterForms{
    priceRange:any;
    category_filter_parameter_id:any;
}

export interface accoutnInformation{
    first_name:string;
    middle_name:string;
    last_name:string;
    contact_number:number;
    phone_no:number;
    email_id:string;
    current_password:string;
    new_password:string;
    confirm_password:string;
    change_password_check:boolean;
}

export interface addressDetails{
    addressId:string;
    billAddressTitle:string;
    billName:string;
    billEmail:string;
    billAddress1:string;
    billAddress2:string;
    billLandmark:string;
    billCity:string;
    billState:string;
    billCountry:string;
    billZipcode:string;
    billPhone:string;
    sameaddressFlag:string;
    shipAddressTitle:string;
    shipName:string;
    shipEmail:string;
    shipAddress1:string;
    shipAddress2:string;
    shipLandmark:string;
    shipCity:string;
    shipState:string;
    shipCountry:string;
    shipZipcode:string;
    shipPhone:string;
}

interface loginForm{
    username:string;
    password:string;
}

interface registerForm{
    first_name:string;
    middle_name:string;
    last_name:string;
    contact_number:number;
    email_id:string;
    password:string;
    cnf_password:string;
}