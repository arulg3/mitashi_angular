import { Directive, ElementRef , HostListener , Input, AfterViewInit, Output, EventEmitter, Self } from '@angular/core';
import { ProductsComponent } from '../components/products/products.component';
declare var $: any;
@Directive({
  selector: '[icheck]'
})
export class IcheckDirective {
  @Output() callParentMethod = new EventEmitter();
  $: any = $;
  self = this;
  constructor(
    el: ElementRef,
    private products:ProductsComponent
  ) {
    const currentobj = this;
    this.$(el.nativeElement).iCheck({
      checkboxClass: 'iradio_flat-green',
      radioClass: 'iradio_flat-green',
      labelHover: true
    }).on('ifChecked', function(event) {
      let filter_name = $(this).attr("filter-name");
      if(filter_name == "priceFilter"){
        localStorage.removeItem(filter_name);
      }
      let val = $(this).val();
      if (localStorage.getItem(filter_name) === null) {
        const filter_array = [];
        if(filter_array.indexOf(val) === -1){
          filter_array.push(val);
        }
        localStorage.setItem(filter_name, JSON.stringify(filter_array));
      }else if(localStorage.getItem(filter_name) !== null){
        const filter_array = JSON.parse(localStorage.getItem(filter_name));
        if(filter_array.indexOf(val) === -1){
          filter_array.push(val);
        }
        localStorage.setItem(filter_name, JSON.stringify(filter_array));
      }
      currentobj.products.filterfunction();
    }).on('ifUnchecked',function(event) {
      let filter_name = $(this).attr("filter-name");
      let val = $(this).val();
      const filter_array = JSON.parse(localStorage.getItem(filter_name));
      if(filter_array !== null){
        if(filter_array.length > 0){
          const index: number = filter_array.indexOf(val);
          if (index !== -1) {
            filter_array.splice(index, 1);
          }   
          localStorage.setItem(filter_name, JSON.stringify(filter_array));
        }
      }
      currentobj.products.filterfunction();
    })
  }
}
