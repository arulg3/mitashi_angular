import { TestBed, async, inject } from '@angular/core/testing';

import { AuthMyaccountGuard } from './auth-myaccount.guard';

describe('AuthMyaccountGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthMyaccountGuard]
    });
  });

  it('should ...', inject([AuthMyaccountGuard], (guard: AuthMyaccountGuard) => {
    expect(guard).toBeTruthy();
  }));
});
