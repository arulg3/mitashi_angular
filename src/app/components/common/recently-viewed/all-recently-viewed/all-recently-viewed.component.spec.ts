import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRecentlyViewedComponent } from './all-recently-viewed.component';

describe('AllRecentlyViewedComponent', () => {
  let component: AllRecentlyViewedComponent;
  let fixture: ComponentFixture<AllRecentlyViewedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRecentlyViewedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRecentlyViewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
