import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InpressComponent } from './inpress.component';

describe('InpressComponent', () => {
  let component: InpressComponent;
  let fixture: ComponentFixture<InpressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InpressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InpressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
