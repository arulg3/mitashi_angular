import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InpressDetailsComponent } from './inpress-details.component';

describe('InpressDetailsComponent', () => {
  let component: InpressDetailsComponent;
  let fixture: ComponentFixture<InpressDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InpressDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InpressDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
