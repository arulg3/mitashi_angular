import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductReviewComponent } from './list-product-review.component';

describe('ListProductReviewComponent', () => {
  let component: ListProductReviewComponent;
  let fixture: ComponentFixture<ListProductReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProductReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
